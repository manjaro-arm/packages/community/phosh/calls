# Maintainer: Philip Müller <philm@manjaro.org>
# Contributor: Dan Johansen
# Contributor: Danct12 <danct12@disroot.org>
# Contributor: Fabian Bornschein <fabiscafe@archlinux.org>
# Contributor: Carl Smedstad <carsme@archlinux.org>
# Contributor: Yassine Oudjana <y.oudjana@protonmail.com>
# Contributor: Philip Goto <philip.goto@gmail.com>
# Contributor: GI Jack <GI_Jack@hackermail.com>

pkgname=gnome-calls
_pkgver=48_beta.1
pkgver=${_pkgver//_/}
pkgrel=1
pkgdesc="Phone dialer and call handler"
arch=(x86_64 armv7h aarch64)
url="https://gitlab.gnome.org/GNOME/calls"
license=(GPL-3.0-or-later)
depends=(
  callaudiod
  dconf
  evolution-data-server
  feedbackd
  folks
  gcc-libs
  glib2
  glibc
  gom
  gstreamer
  gtk4
  hicolor-icon-theme
  libadwaita
  libgee
  libmm-glib
  libpeas-2
  libsecret
  sofia-sip
)
makedepends=(
  appstream
  git
  glib2-devel
  meson
  python-docutils
  vala
)
source=("git+https://gitlab.gnome.org/GNOME/calls.git?signed#tag=v${pkgver/[a-z]/_&}"
        "git+https://gitlab.gnome.org/World/Phosh/libcall-ui.git")
b2sums=('87752c3f62303a4ae9d2e4e8c4dd4491e52deff00368935d45b0228f4ce8cc20e7d98e428b5d1b02ad539f37fddc0da01af382bb782ac58ab088a106962b02a6'
        'SKIP')
validpgpkeys=(
  B9386554B7DD266BCB8E29A990F0C9B18A6B4A19 # Evangelos Ribeiro Tzaras <devrtz@fortysixandtwo.eu>
)

prepare() {
  cd calls

  git submodule init
  git submodule set-url subprojects/libcall-ui "${srcdir}/libcall-ui"
  git -c protocol.file.allow=always -c protocol.allow=never submodule update
}

build() {
  local meson_options=(
    -D gtk_doc=true
    -D tests=false
  )

  arch-meson calls build "${meson_options[@]}"
  meson compile -C build
}

check() {
  meson test -C build --no-rebuild --print-errorlogs
}

package() {
  meson install -C build --no-rebuild --destdir "${pkgdir}"
}

